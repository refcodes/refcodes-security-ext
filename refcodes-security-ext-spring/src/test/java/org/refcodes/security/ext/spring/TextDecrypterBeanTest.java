// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.ext.spring;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.refcodes.factory.BeanLookupFactory;
import org.refcodes.factory.alt.spring.SpringBeanFactory;
import org.refcodes.runtime.SystemProperty;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class TextDecrypterBeanTest {

	private static final String LAST = "Last";
	private static final String FIRST = "First";
	private static final String HALLO_WELT = "Hallo Welt";
	private Map<String, String> _factoryConf;
	private static final String TEST_BEAN = "testBean";

	// /////////////////////////////////////////////////////////////////////////
	// SETUP:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeEach
	public void beforeEach() {
		_factoryConf = new HashMap<>();
		_factoryConf.put( "FIRST_NAME", FIRST );
		_factoryConf.put( "LAST_NAME", LAST );
		final String encryptedText = TextObfuscaterUtility.toEncryptedText( HALLO_WELT );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Text      = " + HALLO_WELT );
			System.out.println( "Encrypted = " + encryptedText );
		}
		assertNotSame( HALLO_WELT, encryptedText );
		_factoryConf.put( "PASSWORD", encryptedText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eKey : _factoryConf.keySet() ) {
				System.out.println( eKey + " = " + _factoryConf.get( eKey ) );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTextDecrypterBean() {
		final TextDecrypterBean theBean = new TextDecrypterBean();
		final String theText = HALLO_WELT;
		final String theEncryptedText = TextDecrypterBean.toEncryptedText( theText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\"" + theText + "\" --> \"" + theEncryptedText + "\"" );
		}
		theBean.setEncryptedText( theEncryptedText );
		assertEquals( theText, theBean.getObject() );
		final String encryptedText = TextObfuscaterUtility.toEncryptedText( HALLO_WELT );
		assertNotSame( HALLO_WELT, encryptedText );
		theBean.setEncryptedText( encryptedText );
		assertEquals( theText, theBean.getObject() );
	}

	@Test
	public void testTextDecrypterContext() throws IOException {
		final Resource theFactoryContextResource = new ClassPathResource( "refcodes-factory-context.xml" );
		BeanLookupFactory<String> theLookupFactory = new SpringBeanFactory( new URI[] { theFactoryContextResource.getURI() }, _factoryConf );
		TestBean theTestBean = theLookupFactory.create( TEST_BEAN );
		assertEquals( theTestBean.getFirstName(), FIRST );
		assertEquals( theTestBean.getLastName(), LAST );
		assertEquals( theTestBean.getPassword(), HALLO_WELT );
	}
}
