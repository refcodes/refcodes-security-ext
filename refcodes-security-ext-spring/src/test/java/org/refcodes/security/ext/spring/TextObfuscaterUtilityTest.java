// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.ext.spring;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class TextObfuscaterUtilityTest {

	private static final String HALLO_WELT = "Hallo Welt";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSymetricCryptography() throws Exception {
		// WARMUP:
		final String theText = HALLO_WELT;
		String theEncryptedText = TextObfuscaterUtility.toEncryptedText( theText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\"" + theText + "\" --> \"" + theEncryptedText + "\"" );
		}
		String theDecryptedText = TextObfuscaterUtility.toDecryptedText( theEncryptedText );
		assertEquals( theText, theDecryptedText );
		assertEquals( theDecryptedText, TextObfuscaterUtility.toDecryptedText( theEncryptedText ) );
		// TESTSTRING:
		final String theTestString = "0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789";
		// ENCRYPTION:
		long theStartTime = System.currentTimeMillis();
		final int theRuns = 1000;
		for ( int i = 0; i < theRuns; i++ ) {
			theEncryptedText = TextObfuscaterUtility.toEncryptedText( ( theTestString + i ) );
		}
		long theEndTime = System.currentTimeMillis();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "SYMETRIC ENCRYPTION:" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theRuns + " / seconds = " + ( ( theEndTime - theStartTime ) / 1000 ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "1 / milliseconds = " + ( ( (double) theEndTime - (double) theStartTime ) / theRuns ) );
		}
		// DECRYPTION:
		theStartTime = System.currentTimeMillis();
		for ( int i = 0; i < theRuns; i++ ) {
			theDecryptedText = TextObfuscaterUtility.toDecryptedText( theEncryptedText );
		}
		theEndTime = System.currentTimeMillis();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "SYMETRIC DECRYPTION:" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theRuns + " / seconds = " + ( ( theEndTime - theStartTime ) / 1000 ) );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "1 / milliseconds = " + ( ( (double) theEndTime - (double) theStartTime ) / theRuns ) );
		}
	}
}
