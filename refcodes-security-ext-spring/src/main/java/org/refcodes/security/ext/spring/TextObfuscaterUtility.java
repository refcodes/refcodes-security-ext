// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.ext.spring;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.Prefix;
import org.refcodes.runtime.SystemContext;
import org.refcodes.security.DecryptionException;
import org.refcodes.security.EncryptionException;
import org.refcodes.security.PasswordTextDecrypter;
import org.refcodes.security.PasswordTextEncrypter;

/**
 * The {@link TextObfuscaterUtility} is a mere in memory text obfuscater (as a
 * dynamically determined {@link SystemContext#HOST_APPLICATION} password is
 * used (which is depended on the current application and the host this
 * application is processed on and may be acquired by accessing and manipulating
 * alongside executing the application on the host or heuristically simulating
 * the application with metrics gathered from the host).
 */
public final class TextObfuscaterUtility {

	public static final int CIPHER_UID_TIMESTAMP_LENGTH = 14;
	public static final int CIPHER_UID_LENGTH = 24;
	public static final int CIPHER_LENGTH = 48;
	public static final int MESSAGE_LENGTH = 256;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( TextObfuscaterUtility.class.getName() );

	private static final PasswordTextEncrypter TEXT_ENCRYPTOR = new PasswordTextEncrypter( SystemContext.HOST_APPLICATION.toContextSequence() );;
	private static final PasswordTextDecrypter TEXT_DECRYPTOR = new PasswordTextDecrypter( SystemContext.HOST_APPLICATION.toContextSequence() );;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Private empty constructor to prevent instantiation as of being a utility
	 * with just static public methods.
	 */
	private TextObfuscaterUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// CAMOUFLAGE
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CAMOUFLAGE
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decrypt the given text in case it has the prefix identifying the text as
	 * being encrypted, else the plain text is returned and a warning is printed
	 * out that the passed text is not encrypted (just the first two or three
	 * chars or zero chars are shown depending on the length of the text).
	 * -------------------------------------------------------------------------
	 * Attention: A unique system TID is used for decryption. Decryption with
	 * this method will only work on the same system used for encryption via
	 * {@link #toEncryptedText(String)}!
	 * -------------------------------------------------------------------------
	 * This method is most useful for encrypting passwords or authentication
	 * credentials in configuration files on live servers to provide at least a
	 * minimum of security.
	 * 
	 * @param aText The text being decrypted.
	 * 
	 * @return The decrypted text.
	 */
	public static String toDecryptedText( String aText ) {
		if ( aText.toLowerCase().startsWith( Prefix.ENCRYPTED.getPrefix() ) ) {
			final String theText = aText.substring( Prefix.ENCRYPTED.getPrefix().length() );
			try {
				return TEXT_DECRYPTOR.toDecrypted( theText );
			}
			catch ( DecryptionException e ) {
				throw new IllegalStateException( "Cannot decrypt text as of the decryptor's <" + TEXT_DECRYPTOR.getClass().getName() + ">  illegal state!", e );
			}
		}
		String theHint = "";
		if ( aText.length() > 12 ) {
			theHint = "and starting with first three characters \"" + aText.substring( 0, 3 ) + "...\" ";
		}
		LOGGER.log( Level.WARNING, "A text with length <" + aText.length() + "> " + theHint + "has been provided which is not encrypted. An encrypted text must start with \"" + Prefix.ENCRYPTED.getPrefix() + "\"!" );
		return aText;
	}

	/**
	 * Encrypts the given text and prepends a prefix identifying the text as
	 * being encrypted.
	 * -------------------------------------------------------------------------
	 * Attention: A unique system TID is used for encryption. Decryption with
	 * the {@link #toDecryptedText(String)} method will only work on the same
	 * system used for encryption via this method!
	 * -------------------------------------------------------------------------
	 * This method is most useful for encrypting passwords or authentication
	 * credentials in configuration files on live servers to provide at least a
	 * minimum of security.
	 * 
	 * @param aText The text to be encrypted.
	 * 
	 * @return The encrypted text with the encryption identifier prefixed.
	 */
	public static String toEncryptedText( String aText ) {
		try {
			return Prefix.ENCRYPTED.getPrefix() + TEXT_ENCRYPTOR.toEncrypted( aText );
		}
		catch ( EncryptionException e ) {
			throw new IllegalStateException( "Cannot encrypt text as of the encryptor's <" + TEXT_ENCRYPTOR.getClass().getName() + ">  illegal state!", e );
		}

	}
}
