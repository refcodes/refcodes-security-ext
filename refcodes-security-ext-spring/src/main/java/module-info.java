module org.refcodes.security.ext.spring {
	requires org.refcodes.security;
	requires transitive spring.beans;
	requires org.refcodes.runtime;

	exports org.refcodes.security.ext.spring;
}
