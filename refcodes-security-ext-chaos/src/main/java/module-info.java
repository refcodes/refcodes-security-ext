module org.refcodes.security.ext.chaos {
	requires transitive org.refcodes.exception;
	requires org.refcodes.numerical;
	requires org.refcodes.security;
	requires org.refcodes.security.alt.chaos;
	requires org.refcodes.runtime;

	exports org.refcodes.security.ext.chaos;
}
