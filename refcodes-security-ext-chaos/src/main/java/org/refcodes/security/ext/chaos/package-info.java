// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact extends the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-security">refcodes-security</a>
 * toolkit with <a href=
 * "https://en.wikipedia.org/wiki/Java_Cryptography_Architecture">JCA</a> (Java
 * Cryptography Architecture) support as of the
 * {@link org.refcodes.security.ext.chaos.ChaosProvider}, the
 * {@link org.refcodes.security.ext.chaos.ChaosKeyGenerator} and the
 * {@link org.refcodes.security.ext.chaos.ChaosCipherSpi} types.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-security"><strong>refcodes-security:
 * Chaos-based encryption as Java cryptographic extension (and
 * without)</strong></a> documentation for an up-to-date and detailed
 * description on the usage of this artifact.
 * </p>
 * 
 * Thanks to Christian Pontesegger's very good examples on <a href=
 * "http://codeandme.blogspot.de/2013/07/writing-your-own-jca-extensions-full.html">Writing
 * your own JCA extensions - a full cipher</a> and <a href=
 * "http://codeandme.blogspot.de/2013/06/writing-your-own-jca-extensions-simple.html">Writing
 * your own JCA extensions - a simple digest</a>!
 */
package org.refcodes.security.ext.chaos;