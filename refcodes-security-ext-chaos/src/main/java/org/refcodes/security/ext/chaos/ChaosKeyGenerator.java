// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.ext.chaos;

import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.KeyGeneratorSpi;
import javax.crypto.SecretKey;

import org.refcodes.numerical.NumericalUtility;
import org.refcodes.security.alt.chaos.ChaosKey;

/**
 * Thanks Christian Pontesegger for the very good example on "Writing your own
 * JCA extensions - a full cipher" at:
 * "http://codeandme.blogspot.de/2013/07/writing-your-own-jca-extensions-full.html"
 * and for the very good example on "Writing your own JCA extensions - a simple
 * digest " at:
 * "http://codeandme.blogspot.de/2013/06/writing-your-own-jca-extensions-simple.html"
 */
public class ChaosKeyGenerator extends KeyGeneratorSpi {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	SecureRandom _randomSource;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void engineInit( SecureRandom aSecureRandom ) {
		_randomSource = aSecureRandom;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void engineInit( AlgorithmParameterSpec ap, SecureRandom sr ) throws InvalidAlgorithmParameterException {
		throw new InvalidAlgorithmParameterException( "No parameters supported in this class" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SecretKey engineGenerateKey() {
		if ( _randomSource == null ) {
			_randomSource = new SecureRandom();
		}
		final double x0 = _randomSource.nextDouble();
		final double a = NumericalUtility.toScaled( _randomSource.nextDouble(), 3.57, 4 );
		final long s = _randomSource.nextLong();
		return new ChaosKey( x0, a, s );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void engineInit( int aKeySize, SecureRandom aSecureRandom ) {
		engineInit( aSecureRandom );
	}
}
