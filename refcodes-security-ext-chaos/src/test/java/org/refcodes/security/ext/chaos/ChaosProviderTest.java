// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.security.ext.chaos;

import static org.junit.jupiter.api.Assertions.*;
import java.security.Provider;
import java.security.Security;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.alt.chaos.ChaosKey;

/**
 * Thanks Christian Pontesegger for the very good example on "Writing your own
 * JCA extensions - a full cipher" at:
 * "http://codeandme.blogspot.de/2013/07/writing-your-own-jca-extensions-full.html"
 * and for the very good example on "Writing your own JCA extensions - a simple
 * digest " at:
 * "http://codeandme.blogspot.de/2013/06/writing-your-own-jca-extensions-simple.html"
 */
public class ChaosProviderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testChaosProvider() {
		final int theIndex = Security.addProvider( new ChaosProvider() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( Provider eProvider : Security.getProviders() ) {
				System.out.println( eProvider.getName() + " - " + eProvider.getInfo() );
			}
		}
		assertTrue( theIndex >= 0 );
		final String theMessage = "Hello world, chaos is here to stay!";
		try {
			final Cipher c = Cipher.getInstance( ChaosKey.PROVIDER_NAME, new ChaosProvider() );
			final SecretKey key = new ChaosKey( 0.67, 3.61, 12536 );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Message:   " + theMessage );
			}
			c.init( Cipher.ENCRYPT_MODE, key );
			final byte[] encrypted = c.doFinal( theMessage.getBytes() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Encrypted: " + new String( encrypted ) );
			}
			assertNotEquals( theMessage, new String( encrypted ) );
			c.init( Cipher.DECRYPT_MODE, key );
			final byte[] decrypted = c.doFinal( encrypted );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Decrypted: " + new String( decrypted ) );
			}
			assertEquals( theMessage, new String( decrypted ) );
		}
		catch ( Exception e ) {
			e.printStackTrace();
			System.out.println( "To get it up and running on your local machine outside a sigend JAR, go as follows: https://www.sureshpunmagar.com.np/2017/06/how-to-fix-javalangsecurityexception.html" );
		}
	}
}
